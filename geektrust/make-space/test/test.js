const assert = require('assert');
const { MakeSpace } = require('../src/MakeSpace');
const { main } = require('../src/geektrust');
const { ERROR_CODES } = require('../src/config.js');
// let mySpaceInstance;

// beforeEach(function() {
//   mySpaceInstance = new MySpace();
// });

describe('Make Space', function () {
  const { testData } = require('../test/testData.json');
  /**
   * RULES
   * Bookings can be made only in a single day from 00:00 to night 23:45
   * A booking can be started and ended only in 15 minute intervals
   * The rooms will be allocated only to those who book them, on a first come first serve basis
   * The most optimal room which can accommodate the number of people will be allocated
   * In case if the room of desired capacity is not available, the next available capacity room will be allocated
   * No meetings can be scheduled during the buffer time
   * Bookings can be only made for 2 or more people and upto a maximum of 20 people
   * Time input should follow HH:MM format
   */

  describe('Input validity', function () {
    describe('15 minute intervals slots', function () {
      let makeSpaceInstance = new MakeSpace();
      let inputs = [
        'VACANCY 15:35 16:00',
        'BOOK 15:35 16:00 12',
        'VACANCY 15:30 16:25',
        'BOOK 15:30 16:25 12',
        'VACANCY 13:25 15:35',
        'BOOK 13:25 15:35 12',
      ];
      inputs.forEach((input) => {
        it(`${input} should return '${ERROR_CODES.invalidInput}'`, function () {
          assert.equal(
            makeSpaceInstance.processLine(input),
            ERROR_CODES.invalidInput
          );
        });
      });
    });

    describe('No meetings in buffer time', function () {
      let makeSpaceInstance = new MakeSpace();
      let inputs = [
        'VACANCY 09:00 13:00',
        'BOOK 09:00 13:00 12',
        'VACANCY 12:30 14:00',
        'BOOK 12:30 14:00 20',
      ];
      inputs.forEach((input) => {
        it(`${input} should return '${ERROR_CODES.noVacency}'`, function () {
          assert.equal(
            makeSpaceInstance.processLine(input),
            ERROR_CODES.noVacency
          );
        });
      });
    });

    describe('Only 2-20 people meeting bookings', function () {
      let makeSpaceInstance = new MakeSpace();
      let inputs = [
        'BOOK 09:00 13:00',
        'BOOK 09:00 13:00 1',
        'BOOK 12:30 14:00 30',
      ];
      inputs.forEach((input) => {
        it(`${input} should return '${ERROR_CODES.noVacency}'`, function () {
          assert.equal(
            makeSpaceInstance.processLine(input),
            ERROR_CODES.noVacency
          );
        });
      });
    });
  });

  describe('Continuous and Concurrent Bookings', function () {
    let makeSpaceInstance = new MakeSpace();
    let data = [
      {input:'VACANCY 00:00 02:30',output:"C-Cave D-Tower G-Mansion"},
      {input:'BOOK 00:00 02:30 2',output:"C-Cave"},
      {input:'VACANCY 00:00 02:30',output:"D-Tower G-Mansion"},
      {input:'BOOK 00:00 02:30 5',output:"D-Tower"},
      {input:'BOOK 02:30 03:30 3',output:"C-Cave"},
      {input:'BOOK 02:30 04:30 3',output:"D-Tower"},
      {input:'BOOK 02:30 03:30 3',output:"G-Mansion"},
      {input:'VACANCY 02:30 04:30',output:"NO_VACANT_ROOM"},
      {input:'BOOK 03:30 03:30 3',output:"INCORRECT_INPUT"},
      {input:'BOOK 03:00 03:30 3',output:"NO_VACANT_ROOM"},
      {input:'BOOK 03:30 04:00 3',output:"C-Cave"},
      {input:'VACANCY 04:30 05:30',output:"C-Cave D-Tower G-Mansion"},
      {input:'BOOK 04:30 05:30 2',output:"C-Cave"},
    ];

    data.forEach(({ input, output }) => {
      it(`${input} should return '${output}'`, function () {
        assert.equal(makeSpaceInstance.processLine(input), output);
      });
    });
  });

  describe('First Come First Serve Room booking logic', function () {
    let makeSpaceInstance = new MakeSpace();
    data = [
      { input: 'VACANCY 12:00 13:00', output: 'C-Cave D-Tower G-Mansion' },
      { input: 'BOOK 12:00 13:00 3', output: 'C-Cave' },
      { input: 'VACANCY 12:00 13:00', output: 'D-Tower G-Mansion' },
      { input: 'BOOK 12:00 13:00 3', output: 'D-Tower' },
      { input: 'VACANCY 12:00 13:00', output: 'G-Mansion' },
      { input: 'BOOK 12:00 13:00 3', output: 'G-Mansion' },
      { input: 'VACANCY 12:00 13:00', output: 'NO_VACANT_ROOM' },
      { input: 'BOOK 12:00 13:00 3', output: 'NO_VACANT_ROOM' },
    ];
    data.forEach(({ input, output }) => {
      it(`${input} should return '${output}'`, function () {
        assert.equal(makeSpaceInstance.processLine(input), output);
      });
    });
  });

  // test cases with multiple steps
  testData.forEach((scenario) => {
    describe(`${scenario.name}`, function () {
      let makeSpaceInstance = new MakeSpace();
      scenario.steps.forEach((step) => {
        it(`input '${step.input}' should return '${step.output}'`, function () {
          assert.equal(makeSpaceInstance.processLine(step.input), step.output);
        });
      });
    });
  });

});
