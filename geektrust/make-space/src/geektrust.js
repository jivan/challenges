const fs = require('fs');
const { ERROR_CODES } = require('../src/config.js');
const { MakeSpace } = require('../src/MakeSpace.js');

const filename = process.argv[2];
let makeSpaceInstance = new MakeSpace();

fs.readFile(filename, 'utf8', (err, data) => {
  if (err) {
    console.error(ERROR_CODES.invalidInput);
    return;
  }
  return data
    .split(/\r?\n/)
    .forEach((line) => console.log(makeSpaceInstance.processLine(line)));
});
