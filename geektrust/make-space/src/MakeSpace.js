const {
  bookOperation,
  vacencyOperation,
  ERROR_CODES,
  ROOM_CONFIG,
} = require('../src/config.js');

const { InputLine } = require('../src/InputLine.js');
const { Room } = require('../src/Room.js');

class MakeSpace {
  constructor() {
    this.rooms = [...ROOM_CONFIG]
      .sort((c1, c2) => c1.size - c2.size)
      .map((config) => new Room(config.name, config.size));
    this.addBufferTime();
  }

  addBufferTime() {
    this.rooms.forEach((r) => r.addBufferTime());
  }

  checkAvailablilty(line) {
    if (!line.valid) {
      return ERROR_CODES.invalidInput;
    }

    let availableRooms = this.rooms
      .filter((room) => room.slotAvailable(line.meetingSlot))
      .map((room) => room.name);

    if (availableRooms.length > 0) {
      return availableRooms.join(' ').trim();
    }

    return ERROR_CODES.noVacency;
  }

  bookMeeting(line) {
    if (!line.valid) {
      return ERROR_CODES.invalidInput;
    }
    if (line.people <2 || line.people >20) {
      return ERROR_CODES.noVacency;
    }

    let rooms = this.rooms.filter((room) => room.sizeAvailable(line.people));

    for (const room of rooms) {
      if (room.bookSlot(line.meetingSlot)) {
        return room.name;
      }
    }

    return ERROR_CODES.noVacency;
  }

  processLine(input) {
    let line = new InputLine(input);
    if (!line.valid) {
      return ERROR_CODES.invalidInput;
    }

    if (line.type === bookOperation) return this.bookMeeting(line);
    if (line.type === vacencyOperation) return this.checkAvailablilty(line);
    return ERROR_CODES.invalidInput;
  }

  printAllSchedules() {
    this.cave.printSchedule();
    this.tower.printSchedule();
    this.mansion.printSchedule();
  }
}

exports.MakeSpace = MakeSpace;
