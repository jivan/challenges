const { OPERATIONS } = require('../src/config.js');
const { BookingSlot } = require('../src/BookingSlot.js');

class InputLine {
  constructor(line) {
    [this.type, this.from, this.to, this.people] = line.split(' ');
    if(typeof this.people === typeof undefined) this.people = 0;
  }

  get valid() {
    const validMinuteValues = ['00', '15', '30', '45'];
    const validateSlot = (h, m) =>
      h < 0 || h > 23 || validMinuteValues.includes(m);
    return (
      OPERATIONS.includes(this.type) & this.meetingSlot.valid &&
      validateSlot(...this.from.split(':')) &
        validateSlot(...this.to.split(':'))
    );
  }

  get meetingSlot() {
    return new BookingSlot(this.from, this.to);
  }
}

exports.InputLine = InputLine;
