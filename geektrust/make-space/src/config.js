const _OPERATIONS = Object.freeze(['BOOK', 'VACANCY']);
const [_bookOperation, _vacencyOperation] = _OPERATIONS;
const _ERROR_CODES = Object.freeze({
  noVacency: 'NO_VACANT_ROOM',
  invalidInput: 'INCORRECT_INPUT',
});

const _ROOM_CONFIG = Object.freeze([
  { name: 'C-Cave', size: 3 },
  { name: 'D-Tower', size: 7 },
  { name: 'G-Mansion', size: 20 },
]);

exports.OPERATIONS = _OPERATIONS;
exports.bookOperation = _bookOperation;
exports.vacencyOperation = _vacencyOperation;
exports.ERROR_CODES = _ERROR_CODES;
exports.ROOM_CONFIG = _ROOM_CONFIG;
