const { BookingSlot } = require('../src/BookingSlot.js');

class Room {
  constructor(name, size) {
    this.name = name;
    this.size = size;
    this.schedule = [];
  }

  sizeAvailable = (size) => size <= this.size;

  slotAvailable = (slot) =>
    this.schedule.filter((s) => s.checkOverlap(slot)).length === 0;

  bookSlot(slot) {
    if (this.slotAvailable(slot)) {
      this.schedule.push(slot);
      return true;
    }
    return false;
  }

  addBufferTime() {
    this.schedule.push(new BookingSlot(...'09:00 09:15'.split(' ')));
    this.schedule.push(new BookingSlot(...'13:15 13:45'.split(' ')));
    this.schedule.push(new BookingSlot(...'18:45 19:00'.split(' ')));
  }

  printSchedule = () => console.log(this.schedule.map((s) => s.toString()));
}

exports.Room = Room;
