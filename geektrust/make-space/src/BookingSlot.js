class BookingSlot {
  constructor(from, to) {
    this.from = parseInt(from.replace(':', ''));
    this.to = parseInt(to.replace(':', ''));
  }

  get valid() {
    return this.from < this.to;
  }
  
  checkOverlap = (slot) => !(this.from >= slot.to || this.to <= slot.from);
  toString = () => this.from + ' - ' + this.to;
}

exports.BookingSlot = BookingSlot;
